/* k_hdr.h
 * Main header file for k_main.c, k_xmlbase.c and k_algorithm.c
 * 
 * George Koskeridis (C) 2016
 * 
  ****************/

/* Headers */
#define _XOPEN_SOURCE 700 //for strerror()
//#define _GNU_SOURCE     //for qsort_r();

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>

#include <assert.h> //#define NDEBUG to disable

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <pthread.h>
#include <curl/curl.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/threads.h>
#include "external/sort_r/sort_r.h"



/* XML URL and file macros */
#define WEB_SERVICE_KINO_LOTTERY_URL "http://applications.opap.gr/DrawsRestServices/kino/"
#define XML_FOLDER "xml"
#define XML_FNAME_PREFIX "kino_lottery"


/* Flags for download_KINO_lotteries() function */
#define MAX_DOWNLOAD_THREADS           30
#define SEQUENTIAL_DOWNLOADS         0x00      //0b00000000
#define PARALLEL_DOWNLOADS           0x01      //0b00000001
#define DELETE_FILES                 0x02      //0b00000010
#define GET_LAST_LOTTERY             0x04      //0b00000100

/* Flags for print_lottery_data() function */
#define P_UNPROCESSED                0x00
#define P_PROCESSED                  0x01


/* Debugging macros */
#ifdef __DBG
# define P_ERR_MSG(err_msg1, err_msg2) fprintf(stderr, "== Error at %s():%d ->" \
                                   " %s %s ==\n", __func__, __LINE__, err_msg1,\
                                   (err_msg2) ? err_msg2 : "\b");

# define P_ERR_INT(err_msg, err_int) fprintf(stderr, "== Error at %s():%d ->" \
                                   " %s %d ==\n", __func__, __LINE__, err_msg, err_int);

# define P_ERRNO() fprintf(stderr, "== Error number %d at %s():%d ->" \
                          " %s ==\n", errno, __func__, __LINE__, strerror(errno));
#else
# define P_ERR_MSG(x, y)
# define P_ERR_INT(x, y)
# define P_ERRNO()
#endif

#define BIT_IS_ENABLED(x, y)   ((x >> y) & 1)


/* Declarations */
typedef struct lottery_data {
    //lottery number
    uint32_t lottery_num;

    //winning numbers from 1-80
    uint8_t results[20];

    //magic number/KINO bonus
    uint8_t magic_number;

    //results[] split in 8 groups of 1-10, 11-20 ... 71-80
    uint8_t winning_tens[8][10];

    //the index of which group the magic number belongs to
    size_t magic_number_group_idx;

    //total numbers in each group (eg. winning_tens_total[0] is the total numbers in winning_tens[0])
    uint8_t winning_tens_total[8];

    //price of each group from winning_tens
    float winning_tens_payoff[8];

    //sum from the payoffs of all groups
    float total_payoff;

    //if true, then the data in this struct is valid, if false then
    //it's invalid and it shouldn't be used anywhere
    bool valid_data;
} lottery_data;


/* k_xmlbase.c */
bool download_xml(const char *url, const char *fname);
bool parse_lottery_xml(const char *fname_toparse, lottery_data *data_tosave);
bool download_KINO_lotteries(uint32_t min, uint32_t max, lottery_data **dat, uint8_t flag);
uint32_t get_last_lottery_number(void);

/* k_algorithm.c */
void print_lottery_data(lottery_data *to_print, size_t data_len, FILE *to_write, int flag);
bool process_lotteries(lottery_data *to_process, size_t data_len);

