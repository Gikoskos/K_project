/* k_xmlget.c
 * 
 * Functions for downloading and manipulating the .xml files
 * 
 * George Koskeridis (C) 2016
 * 
  ****************/

#include "k_hdr.h"

/* Macro error messages for get_url_thrd_func() function */
#define THREAD_LOTTERY_DAT_NULL  0x0002
#define PROGRAM_LOTTERY_DAT_NULL 0x0003
#define DOWNLOAD_XML_FAILED      0x0004
#define PARSE_LOTTERY_XML_FAILED 0x0005
#define UNLINK_FAILED            0x0006
#define GET_URL_THREAD_SUCCESS   0x0000


typedef struct thread_data {
    char url_buf[255];
    char fname_buf[255];
    lottery_data **lottery_dat;
    bool delete_file;
} thread_data;


/* Local functions */
static void *get_xml_thrd_func(void *void_dat);
static void wait_until_threads_finish(uint32_t up_to, pthread_t *thrd_arr, thread_data *thrd_dat);


bool download_xml(const char *url_tofetch, const char *fname_tosave)
{
    if (!url_tofetch || !fname_tosave) {
        errno = EINVAL;
        P_ERRNO();
        return false;
    }

    if (access(fname_tosave, F_OK) != -1) { //return if file already exists
        //errno = EEXIST;
        //P_ERR_MSG("file already exists", fname_tosave);
        return true;
    }

    bool retvalue = false;


    errno = 0;
    FILE *file = fopen(fname_tosave, "w");

    if (file) {
        CURL *curl = curl_easy_init();

        if (curl) {
            char errbuf[CURL_ERROR_SIZE];
            CURLcode res;

            curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errbuf);
            errbuf[0] = '\0';

            curl_easy_setopt(curl, CURLOPT_HEADER, 0L);
            curl_easy_setopt(curl, CURLOPT_URL, url_tofetch);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);

            if ((res = curl_easy_perform(curl)) != CURLE_OK) {
                size_t len = strlen(errbuf);

                if (len) {
                    if (errbuf[len - 1] == '\n') errbuf[len - 1] = '\0';
                    P_ERR_MSG(errbuf, NULL);
                } else {
                    P_ERR_MSG(curl_easy_strerror(res), url_tofetch);
                }
            } else {
                retvalue = true;
            }

            curl_easy_cleanup(curl);
        } else {
            P_ERR_MSG("curl_easy_init() failed!", NULL);
        }

        (void)fclose(file);

    } else {
        P_ERRNO();
    }

    return retvalue;
}

bool parse_lottery_xml(const char *fname_toparse, lottery_data *data_tosave)
{
    bool retvalue = false;
    xmlDocPtr doc = NULL;

    if (!fname_toparse || !data_tosave) {
        errno = EINVAL;
        P_ERRNO();
        goto RET;
    }

    if (!(doc = xmlParseFile(fname_toparse))) {
        P_ERR_MSG("xmlParseFile() failed to parse file", fname_toparse);
        goto RET;
    }

    xmlNodePtr curr;

    if (!(curr = xmlDocGetRootElement(doc))) {
        P_ERR_MSG("xmlDocGetRootElement() empty document", fname_toparse);
        goto RET;
    }

    if (xmlStrcmp(curr->name, (const xmlChar*)"draw")) {
        P_ERR_MSG("Wrong type document", fname_toparse);
        goto RET;
    }

    xmlChar *key = NULL;
    int j = 0;
    for (curr = curr->xmlChildrenNode; curr; curr = curr->next) {
        if (!xmlStrcmp(curr->name, (const xmlChar*)"drawNo")) {
            key = xmlNodeListGetString(doc, curr->xmlChildrenNode, 1);
            data_tosave->lottery_num = (uint32_t)atoi((const char*)key);
            xmlFree(key);
            continue;
        }

        if (!xmlStrcmp(curr->name, (const xmlChar*)"result")) {
            key = xmlNodeListGetString(doc, curr->xmlChildrenNode, 1);
            data_tosave->results[j++] = (uint32_t)atoi((const char*)key);
            xmlFree(key);
        }
    }
    retvalue = true;

RET:
    if (doc) xmlFreeDoc(doc);

    return retvalue;
}

static void *get_xml_thrd_func(void *void_dat)
{
    thread_data *dat = (thread_data*)void_dat;
    uint8_t max_download_retries = 5, curr_try = 1;
    //boolean flag, indicating success in downloading the file and parsing it
    bool download_success = false;

    if (!dat || !dat->lottery_dat)
        return (void*)THREAD_LOTTERY_DAT_NULL;

    if (!(*dat->lottery_dat))
        return (void*)PROGRAM_LOTTERY_DAT_NULL;

    //if the download failed for some reason, we retry until we get it right
    //for a maximum of max_download_retries times
    while ((download_success != true) && (curr_try <= max_download_retries)) {

        //first we try to download the file from the dat->url_buf URL
        if (download_xml(dat->url_buf, dat->fname_buf) == false)
            return (void*)DOWNLOAD_XML_FAILED;

        //now we try to parse it. If parsing was successful, we set the success flag to be true
        if (parse_lottery_xml(dat->fname_buf, *dat->lottery_dat) == true)
            download_success = true;

        //If parsing failed we delete the file in order to retry again. We also delete the
        //file if parsing was successful and the DELETE_FILES flag was set on the download_KINO_lotteries() call
        if (dat->delete_file || (download_success != true)) {
            if (unlink(dat->fname_buf) == -1) {
                (*(*dat->lottery_dat)).valid_data = download_success;
                return (void*)UNLINK_FAILED;
            }
        }
        curr_try++;
    }

    (*(*dat->lottery_dat)).valid_data = download_success;

    if (download_success != true)
        return (void*)PARSE_LOTTERY_XML_FAILED;
    else
        return (void*)GET_URL_THREAD_SUCCESS;
}

static void wait_until_threads_finish(uint32_t up_to, pthread_t *thrd_arr, thread_data *thrd_dat)
{
    //to get the return value of each thread from pthread_join()
    int32_t *thrd_retvalue = NULL;

    //to get the error code that is returned by pthread_join()
    int32_t pthread_errno;

    for (uint32_t thread_to_wait = 0; thread_to_wait < up_to; thread_to_wait++) {
        pthread_errno = pthread_join(thrd_arr[thread_to_wait], (void**)&thrd_retvalue);

        if (pthread_errno && //pthread_join returns an int != 0 on failure
            thrd_retvalue && //we don't want to accidentally dereference a NULL pointer
            (*thrd_retvalue) != GET_URL_THREAD_SUCCESS) { //if the thread didn't exit successfully

            switch ((*thrd_retvalue)) {
            case THREAD_LOTTERY_DAT_NULL:
                P_ERR_INT("(!dat || !dat->lottery_dat) assertion failed in thread", (int)thread_to_wait);
                break;
            case PROGRAM_LOTTERY_DAT_NULL:
                P_ERR_INT("(!(*dat->lottery_dat)) assertion failed in thread", (int)thread_to_wait);
                break;
            case DOWNLOAD_XML_FAILED:
                P_ERR_INT("download_xml() failed in thread", (int)thread_to_wait);
                break;
            case PARSE_LOTTERY_XML_FAILED:
                P_ERR_INT("parse_lottery_xml() failed in thread", (int)thread_to_wait);
                break;
            case UNLINK_FAILED:
                P_ERR_INT("unlink() failed in thread", (int)thread_to_wait);
                break;
            default:
                break;
            }
        }

        if (thrd_dat[thread_to_wait].lottery_dat) free(thrd_dat[thread_to_wait].lottery_dat);
    }
}

bool download_KINO_lotteries(uint32_t min, uint32_t max, lottery_data **dat, uint8_t flag)
{
    bool retvalue = false;

    if (*dat) {
        P_ERR_MSG("lottery_data **dat is not empty!", NULL);
        goto RET;
    }

    if (min <= max) {
        // calculate total number of lottery results to be downloaded
        size_t total_lotteries = max - min + 1;

        bool del_file = (BIT_IS_ENABLED(flag, 1)) ? true : false;
        uint8_t download_type = BIT_IS_ENABLED(flag, 0);

        //if the total lottery results to be downloaded are less than 2
        //we don't need to have the overhead of concurrency, so we default to sequential
        if (total_lotteries <= 2) download_type = SEQUENTIAL_DOWNLOADS;

        *dat = malloc(sizeof(lottery_data) * (total_lotteries));
        if (!dat) goto RET;

        switch (download_type) {

        /* The parallel download system works like this: There's a maximum allowable number
         * (the MAX_DOWNLOAD_THREADS macro) of threads that can be run at once. If that number is reached
         * then we wait until all the threads, that are currently running, are finished doing their jobs
         * before we can run more threads. At any point in time, there can't be more than
         * MAX_DOWNLOAD_THREADS threads running. */
        case PARALLEL_DOWNLOADS:
            {
                //arguments to pass to each thread
                thread_data *thrd_dat = malloc(sizeof(thread_data) * MAX_DOWNLOAD_THREADS);
                if (!thrd_dat) {
                    P_ERR_MSG("malloc() failed!", NULL);
                    goto RET;
                }

                //pthread_t array to store the identifiers of MAX_DOWNLOAD_THREADS threads
                pthread_t thrd_id[MAX_DOWNLOAD_THREADS];

                //0-based index for thrd_id array to count how many threads are currently running and
                //which thread we're at in the array
                uint32_t current_thread_idx = 0;

                //int to store the return error code of the pthread API functions
                int pthread_errno = 0;


                for (uint32_t curr_lottery = min; curr_lottery <= max; curr_lottery++) {
                    /* Ιf we reached the maximum running thread limit we wait until all threads
                     * are finished, before we start creating any more */
                    if (current_thread_idx >= MAX_DOWNLOAD_THREADS) {
                        wait_until_threads_finish(current_thread_idx, thrd_id, thrd_dat);
                        current_thread_idx = 0;

                        free(thrd_dat);

                        thrd_dat = malloc(sizeof(thread_data) * MAX_DOWNLOAD_THREADS);
                        assert(thrd_dat);
                    }

                    thrd_dat[current_thread_idx].delete_file = del_file;
                    thrd_dat[current_thread_idx].lottery_dat = malloc(sizeof(lottery_data*));
                    assert(thrd_dat[current_thread_idx].lottery_dat);

                    *thrd_dat[current_thread_idx].lottery_dat = (*dat + (curr_lottery - min));

                    snprintf(thrd_dat[current_thread_idx].url_buf, 255, WEB_SERVICE_KINO_LOTTERY_URL"%u.xml", (unsigned int)curr_lottery);
                    snprintf(thrd_dat[current_thread_idx].fname_buf, 255, XML_FOLDER"/"XML_FNAME_PREFIX"%u.xml", (unsigned int)curr_lottery);


                    pthread_errno = pthread_create(&thrd_id[current_thread_idx],
                                                   NULL, get_xml_thrd_func, (void*)&thrd_dat[current_thread_idx]);
                    if (pthread_errno) {
                        errno = pthread_errno;
                        P_ERRNO();
                    } else {
                        current_thread_idx++;
                    }
                }

                /* cleanup the rest of remaining threads if there are any, to fit the structured concurrency model :P */
                wait_until_threads_finish(current_thread_idx, thrd_id, thrd_dat);
                free(thrd_dat);
            }
            retvalue = true;
            break;
        case SEQUENTIAL_DOWNLOADS:
            
            for (uint32_t i = min; i <= max; i++) {
                char url_buf[255], fname_buf[255];

                snprintf(url_buf, 255, WEB_SERVICE_KINO_LOTTERY_URL"%u.xml", (unsigned int)i);
                snprintf(fname_buf, 255, XML_FOLDER"/"XML_FNAME_PREFIX"%u.xml", (unsigned int)i);

                if (!download_xml(url_buf, fname_buf)) {
                    (*(*dat + (i - min))).valid_data = false;
                    continue;
                }

                if (!parse_lottery_xml(fname_buf, (*dat + (i - min)))) {
                    (*(*dat + (i - min))).valid_data = false;
                    continue;
                }

                (*(*dat + (i - min))).valid_data = true;

                errno = 0;
                if (del_file != false) {
                    if (unlink(fname_buf) == -1) {
                        P_ERRNO();
                    }
                }

            }
            retvalue = true;
            break;
        default:
            P_ERR_MSG("wrong flag entered!", NULL);
            break;
        }
    } else {
        P_ERR_MSG("min is bigger than max!", NULL);
    }

RET:
    return retvalue;
}

uint32_t get_last_lottery_number(void)
{
    uint32_t retvalue = 0;
    lottery_data *tmp = malloc(sizeof(lottery_data));

    if (!tmp) {
        P_ERR_MSG("malloc() failed!", NULL);
        goto RET;
    }

    char url_buf[255], fname_buf[255];

    snprintf(url_buf, 255, WEB_SERVICE_KINO_LOTTERY_URL"last.xml");
    snprintf(fname_buf, 255, XML_FOLDER"/"XML_FNAME_PREFIX"last.xml");

    if (!download_xml(url_buf, fname_buf)) {
        P_ERR_MSG("download_xml() failed!", NULL);
        goto RET;
    }

    if (!parse_lottery_xml(fname_buf, tmp)) {
        P_ERR_MSG("parse_lottery_xml() failed!", NULL);
        goto RET;
    }

    errno = 0;
    if (unlink(fname_buf) == -1) {
        P_ERRNO();
    }

    retvalue = tmp->lottery_num;
RET:
    if (tmp) free(tmp);
    return retvalue;
}
