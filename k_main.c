/* k_main.c
 * 
 * Entry point and cleanup functions
 * 
 * George Koskeridis (C) 2016
 * 
  ****************/

#include "k_hdr.h"


lottery_data *lotteries = NULL;


void cleanup(void)
{
    if (lotteries) free(lotteries);

    curl_global_cleanup();

    xmlCleanupThreads();
    xmlCleanupParser();
    xmlUnlockLibrary();

    rmdir(XML_FOLDER);
}

void initialize(void)
{
    setvbuf(stdout, NULL, _IOLBF, 0);

    curl_global_init(CURL_GLOBAL_ALL);

    xmlInitParser();
    xmlInitThreads();

    errno = 0;
    if (mkdir(XML_FOLDER, S_IRWXU) == -1 && errno != EEXIST) {
        P_ERRNO();
    }
}

int main(int argc, char *argv[])
{
    atexit(cleanup);

    initialize();

    if (argc >= 3 && argc <= 5) {
        uint32_t first, last, last_lottery;
        FILE *out;

        if (!isdigit(argv[1][0]) || !isdigit(argv[2][0]) || !argv[1][0] || !argv[2][0]) {
            errno = EINVAL;
            P_ERRNO();
            goto FAIL;
        }

        errno = 0;
        first = (uint32_t)strtoul(argv[1], NULL, 10);
        if (errno) {
            P_ERRNO();
            goto FAIL;
        }

        errno = 0;
        last = (uint32_t)strtoul(argv[2], NULL, 10);
        if (errno) {
            P_ERRNO();
            goto FAIL;
        }

        if (first > last) {
            uint32_t tmp = first;
            first = last;
            last = tmp;
        }

        last_lottery = get_last_lottery_number();
        if (last > last_lottery) {
            fprintf(stderr, "Max KINO games played so far %u\n", (unsigned int)last_lottery);
            goto FAIL;
        }

        int d_flags = PARALLEL_DOWNLOADS;

        if (argc >= 4) {
            d_flags |= DELETE_FILES;

            if (argc > 4) {
                char filename_buff[255];
                snprintf(filename_buff, 255, "%s", argv[4]);
                out = fopen(filename_buff, "w");
            } else {
                out = stdout;
            }
        } else {
            out = stdout;
        }

        if (download_KINO_lotteries(first, last, &lotteries, d_flags) == false) {
            P_ERR_MSG("download_KINO_lotteries() failed!", NULL);
            goto FAIL;
        }

        //print_lottery_data(lotteries, last - first, P_UNPROCESSED);

        if (process_lotteries(lotteries, last - first) == false) {
            P_ERR_MSG("process_lotteries() failed!", NULL);
            goto FAIL;
        }

        print_lottery_data(lotteries, last - first, out, P_PROCESSED);
        fclose(out);
        exit(EXIT_SUCCESS);
    }

FAIL:
    exit(EXIT_FAILURE);
}
