CC = gcc
CFLAGS = -Wall -std=gnu11 `xml2-config --cflags`

LINKER = `curl-config --libs` `xml2-config --libs` -pthread

DBGFLAGS = -D__DBG

SRC := k_main.c k_algorithm.c k_xmlbase.c
#$(DBGFLAGS) 
kino_p: $(SRC)
	$(CC) $(CFLAGS) -O3 $(DBGFLAGS) $^ -o $@ $(LINKER)

.PHONY: clean
clean:
	@rm -f *.o kino_p

rebuild: clean kino_p
